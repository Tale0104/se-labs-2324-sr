from multiprocessing.sharedctypes import Value
from unicodedata import name
from query_handler_base import QueryHandlerBase
import requests
import json


class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "max_power" in query:
            return True
        return False


    def process(self, query):

        try:
            result = self._api()
            text = result("value")
            self.ui.say(f"{text}")
        except:
            self.ui.say("I can handle this request but I won't!!!")


    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        queryString = {}



        headers = {
	        "X-RapidAPI-Key": "14cbce0193msh8dbeffd04ff8471p165546jsn2c6f14e4760a",
	        "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        print(response.json())  