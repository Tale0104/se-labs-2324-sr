from multiprocessing.sharedctypes import Value
from unicodedata import name
from query_handler_base import QueryHandlerBase
import requests
import json


class JokesHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "jokes" in query:
            return True
        return False


    def process(self, query):
        names = query.split()
        term = names 

        try:
            result = self._api()
            text = result("Hope you found what youe were looking for")
            self.ui.say(f"Your word was  {term}")
        except:
            self.ui.say("Oh nou! There was an error trying to contact urban dictionary api.")
            self.ui.say("Try something else!")

    def call_api(self):
        url = "https://world-of-jokes1.p.rapidapi.com/v1/jokes/categories"

        queryString = {}



        headers = {
	        "X-RapidAPI-Key": "14cbce0193msh8dbeffd04ff8471p165546jsn2c6f14e4760a",
	        "X-RapidAPI-Host": "world-of-jokes1.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        print(response.json())  