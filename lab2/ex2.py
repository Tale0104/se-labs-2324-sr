import csv

employee = []
office = []

with open('.\lab2\ex2-text.csv', "r") as file:
    reader = csv.reader(file, delimiter=",")
    for row in reader:
        employee.append(row[0] + ", " + row[1])
        office.append(row[0] + ", " + row[3])

with open('.\lab2\ex2-employees.txt', "w") as f:
    for item in employee:
        f.write("\n%s" %item)

with open('.\lab2\ex2-locations.txt', "w") as f:
    for item in office:
        f.write("\n%s" %item)